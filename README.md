# VTGP Projection Game

Fall game jam "projection" theme. 48 hours total.

Player vs. player platformer where players can "project" disadvantages to hinder the opponent. See commits for progress details.

There are still several bugs involving the pause menu and power-ups, but the minimum viable product is functional.

To try it out, download the Build folder and run the game executable.

Controls:


Player 1:

A - Move Left

D - Move Right

W - Jump

E - Use Projection Power-Up


Player 2:

J - Move Left

L - Move Right

I - Jump

U - Use Projection Power-Up