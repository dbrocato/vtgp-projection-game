﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Spike Power Up")]
public class SpikePowerUp : PowerUp {

	public GameObject spikePrefab;
	public float dropHeight;
	public float dropDistanceFromPlayer;

	public override void Cast(GameObject target) {
		Vector3 spawnCoordinates = new Vector3(target.transform.position.x + dropDistanceFromPlayer, target.transform.position.y + dropHeight, 0);
		Instantiate(spikePrefab, spawnCoordinates, Quaternion.Euler(0, 0, 180));
	}
}
