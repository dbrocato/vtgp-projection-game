﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Spawn Enemy Power Up")]
public class SpawnEnemyPowerUp : PowerUp {

	public GameObject enemyPrefab;
	public float spawnDisplacement;

	public LayerMask mask;

	public override void Cast(GameObject target) {
		Vector3 spawnPosition = new Vector3(target.transform.position.x + spawnDisplacement, target.transform.position.y, 0);
		RaycastHit2D hit = Physics2D.Raycast(target.transform.position, Vector2.right, spawnDisplacement, mask);
		if (hit) {
			spawnPosition.Set(spawnPosition.x, spawnPosition.y + 3, 0);
		}
		Instantiate(enemyPrefab, spawnPosition, Quaternion.identity);
	}
}
