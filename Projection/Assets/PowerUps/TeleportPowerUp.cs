﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Teleport Power Up")]
public class TeleportPowerUp : PowerUp {

	//public GameObject windSprite;
	public float displacement;

	private Vector3 newPosition;

	public TeleportPowerUp() {
		newPosition = new Vector3(0, 0, 0);
	}

	public override void Cast(GameObject target) {
		newPosition.Set(target.transform.position.x + displacement, target.transform.position.y, 0);
		target.transform.position = newPosition;

		if (target.tag.Equals("Red")) {
			ScoreTracker.BlueScore++;
			ScoreTracker.RedScore--;
		}
		if (target.tag.Equals("Blue")) {
			ScoreTracker.BlueScore--;
			ScoreTracker.RedScore++;
		}
	}
}
