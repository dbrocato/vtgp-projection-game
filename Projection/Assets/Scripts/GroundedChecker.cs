﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundedChecker : MonoBehaviour {
	
	private bool grounded;
	public LayerMask mask; // LayerMask to only check Ground layer.
	private Vector3 correction;

	// Use this for initialization
	void Awake () {
		grounded = false;
		correction = new Vector3(0, -0.4f, 0); // Correction for pivot point. Moves Raycast origin near feet.
	}

	// Update is called once per frame
	void FixedUpdate () {
		RaycastHit2D hit = Physics2D.Raycast(transform.position - correction, -Vector2.up, 1, mask);
		if (hit.collider != null) {
			grounded = true;
		}
		else {
			grounded = false;
		}
	}

	public bool isGrounded() {
		return grounded;
	}
}
