﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedInputReader : MonoBehaviour {

	private float x;
	private float lastX;
	private float rawX;
	private float lastRawX;

	// Use this for initialization
	void Start () {
		x = 0;
		lastX = 1;
		rawX = 0;
		lastRawX = 1;
	}
	
	// Update is called once per frame
	void Update () {
		rawX = Input.GetAxisRaw("Red");
		if (rawX != 0) {
			lastRawX = rawX;
		}
		x = Input.GetAxis("Red");
		if (x != 0) {
			lastX = x;
		}
	}

	public float getRawX() {
		return rawX;
	}

	public float getLastRawX() {
		return lastRawX;
	}

	public float getX() {
		return x;
	}

	public float getLastX() {
		return lastX;
	}
}