﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedPlayerController : MonoBehaviour {

	public float moveSpeed;
	public float jumpSpeed;
	public float slideSpeed;

	private float slowAmount = 4;
	
	private bool isSlowed;
	private Rigidbody2D rb;
	private Vector2 vel;
	private GroundedChecker gc;
	private bool sliding;

	// Use this for initialization
	void Awake () {
		rb = GetComponent<Rigidbody2D>();
		gc = GetComponent<GroundedChecker>();
		vel = new Vector2(0, 0);
	}
	
	// Update is called once per frame
	void Update () {
		float x = Input.GetAxis("Red");
		if (!sliding) {
			if (isSlowed) {
				vel.Set(x * moveSpeed/slowAmount, rb.velocity.y);
			}
			else {
				vel.Set(x * moveSpeed, rb.velocity.y);
			}
			rb.velocity = vel;
		}
		if (Input.GetKeyDown(KeyCode.W) && gc.isGrounded() && !sliding) {
			vel.Set(rb.velocity.x, jumpSpeed);
			rb.velocity = vel;
		}
		//if (Input.GetKeyDown(KeyCode.LeftControl) && gc.isGrounded() && !isSlowed) {
		//	StartCoroutine("Slide");
		//}
	}
/*
	public IEnumerator Slide() {
		float x = Input.GetAxis("Red");
		vel.Set(x * slideSpeed, rb.velocity.y);
		rb.velocity = vel;
		rb.drag = 10;
		while (rb.velocity.x != 0) {
			yield return new WaitForFixedUpdate();
			sliding = true;
		}
		sliding = false;
		rb.drag = 0;
		yield return 0;
	}
*/

	public void setSlowed(bool slowed) {
		isSlowed = slowed;
	}
}
