﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedAnimationChooser : MonoBehaviour {

	private Animator anim;
	private float x;
	private float lastX;
	private GroundedChecker gc;
	private RedInputReader ir;

	// Use this for initialization
	void Awake () {
		anim = GetComponent<Animator>();
		//gc = GetComponent<GroundedChecker>();
		ir = GetComponent<RedInputReader>();

		anim.SetFloat("X", 1);
		anim.SetFloat("lastX", 1);
	}
	
	// Update is called once per frame

	// TODO: Implement use of InputReader class.
	void Update () {
		
		x = ir.getRawX();
		anim.SetFloat("X", x);

		if (x != 0) {
			
			anim.SetBool("Moving", true);

			lastX = x;
			anim.SetFloat("lastX", lastX);
		}
		else {
			anim.SetBool("Moving", false);
		}
	}
}
