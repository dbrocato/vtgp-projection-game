﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mapping : MonoBehaviour {

	public GameObject red;
	public GameObject blue;

	public GameObject redDot;
	public GameObject blueDot;

	private RectTransform redDotT;
	private RectTransform blueDotT;

	Vector2 redPos;
	Vector2 bluePos;

	// Use this for initialization
	void Awake () {
		redPos = new Vector2 (-250, 5);
		bluePos = new Vector2 (-250, -5);

		redDotT = redDot.GetComponent<RectTransform>();
		blueDotT = blueDot.GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
		redPos.Set(3.3f * red.transform.position.x - 250, 5);
		redDotT.anchoredPosition = redPos;
		bluePos.Set(3.3f * blue.transform.position.x - 250, -5);
		blueDotT.anchoredPosition = bluePos;
	}
}
