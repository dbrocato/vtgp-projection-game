﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScoreTracker : MonoBehaviour {

    public static int BlueScore;
    public static int RedScore;
    public Text BlueScoreText;
    public Text RedScoreText;


	// Use this for initialization
	void Start () {
        BlueScore = 0;
        RedScore = 0;
	}
	
	// Update is called once per frame
	void Update () {
        BlueScoreText.text = "Score: " + BlueScore.ToString();
        RedScoreText.text = "Score: " + RedScore.ToString();
	}
}
