﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuControl : MonoBehaviour {

    public Collider2D level1;
    public Collider2D level2;
    public Collider2D level3;
    public Collider2D level4;

    public Collider2D exit;
    public Collider2D player;

    public string level1name;
    public string level2name;
    public string level3name;
    public string level4name;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (level1.IsTouching(player))
        {
            SceneManager.LoadScene(level1name);
        }
        else if (level2.IsTouching(player))
        {
            SceneManager.LoadScene(level2name);
        }
        else if (level3.IsTouching(player))
        {
            SceneManager.LoadScene(level3name);
        }
        else if (level4.IsTouching(player))
        {
            SceneManager.LoadScene(level4name);
        }
        else if (exit.IsTouching(player))
        {
            Application.Quit();
        }

    }
}
