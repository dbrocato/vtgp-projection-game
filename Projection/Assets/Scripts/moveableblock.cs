﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveableblock : MonoBehaviour {
    private Vector3 velocity;
    private bool moving;
    public float MoveTime;
    private float MoveTimeTracker;
    private int DirectionMarker;
    public GameObject block;
    public int spead;

	// Use this for initialization
	void Start () {
        MoveTimeTracker = MoveTime;
        DirectionMarker = 1;
        velocity = new Vector3(spead, 0, 0);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (MoveTime > 0)
        {
            MoveTime -= 0.1f;
        }
        else
        {
            MoveTime += MoveTimeTracker;
            DirectionMarker = DirectionMarker * (-1);
        }
        block.transform.position += velocity * DirectionMarker * Time.deltaTime;
	}
}
