﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour {

	public float durationOfExistence;
	public float slowDuration;

	// Use this for initialization
	void Awake () {
		StartCoroutine("Exist");
	}
	
	private IEnumerator Exist() {
		yield return new WaitForSeconds(durationOfExistence);
		Destroy(this.gameObject);
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag.Equals("Red")) {
			ScoreTracker.BlueScore++;
			ScoreTracker.RedScore--;
			StartCoroutine("Slow", coll.gameObject);
		}
		if (coll.gameObject.tag.Equals("Blue")) {
			ScoreTracker.BlueScore--;
			ScoreTracker.RedScore++;
			StartCoroutine("Slow", coll.gameObject);
		}
	}

	public IEnumerator Slow(GameObject target) {
		if (target.tag.Equals("Red")) {
			RedPlayerController con = target.GetComponent<RedPlayerController>();
			con.setSlowed(true);
		}
		if (target.tag.Equals("Blue")) {
			BluePlayerController con = target.GetComponent<BluePlayerController>();
			con.setSlowed(true);
		}
		yield return new WaitForSeconds(slowDuration);
		if (target.tag.Equals("Red")) {
			RedPlayerController con = target.GetComponent<RedPlayerController>();
			con.setSlowed(false);
		}
		if (target.tag.Equals("Blue")) {
			BluePlayerController con = target.GetComponent<BluePlayerController>();
			con.setSlowed(false);
		}
	}
}
