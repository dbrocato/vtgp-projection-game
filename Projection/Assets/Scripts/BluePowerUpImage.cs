﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BluePowerUpImage : MonoBehaviour {

	public Sprite questionMark;
	public Sprite spike;
	public Sprite enemy;
	public Sprite teleport;

	public static PowerUp powerUp = null;

	// Use this for initialization
	void Update () {
		if (powerUp == null) {
			this.transform.GetComponent<UnityEngine.UI.Image>().sprite = questionMark;
		}
		else if (powerUp.name.Equals("SpikePowerUp")) {
			this.transform.GetComponent<UnityEngine.UI.Image>().sprite = spike;
		}
		else if (powerUp.name.Equals("SpawnEnemyPowerUp")) {
			this.transform.GetComponent<UnityEngine.UI.Image>().sprite = enemy;
		}
		else if (powerUp.name.Equals("TeleportPowerUp")) {
			this.transform.GetComponent<UnityEngine.UI.Image>().sprite = teleport;
		}
	}
}