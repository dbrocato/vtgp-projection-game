﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpHandler : MonoBehaviour {

	public GameObject target;
	public PowerUp powerUp;
	public KeyCode useKey;

	// Use this for initialization
	void Awake () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(useKey) && powerUp != null) {
			Debug.Log("Ability Cast");
			powerUp.Cast(target);
			powerUp = null;
			if (this.gameObject.tag.Equals("Blue")) {
				BluePowerUpImage.powerUp = powerUp;
			}
			if (this.gameObject.tag.Equals("Red")) {
				RedPowerUpImage.powerUp = powerUp;
			}
		}
	}

	public void setPowerUp(PowerUp powerUp) {
		this.powerUp = powerUp;
		if (this.gameObject.tag.Equals("Blue")) {
			BluePowerUpImage.powerUp = powerUp;
		}
		if (this.gameObject.tag.Equals("Red")) {
			RedPowerUpImage.powerUp = powerUp;
		}
	}
}
