﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sprites : MonoBehaviour {

    private float moveSpeed = 10f;
    private float countdown = 3.0f;
    
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (countdown > 2.0f)
        {
            transform.Translate(Vector2.down * moveSpeed * Time.deltaTime);
            countdown -= Time.deltaTime;
        }
        else if (countdown <= 0.0f)
        {
            countdown = countdown + (float)3.0;

        }
        else
        {
            countdown -= Time.deltaTime;
        }
    }
}
