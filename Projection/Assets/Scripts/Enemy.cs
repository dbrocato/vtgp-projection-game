﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public float burstSpeed;
    public float moveCooldown;
    public float numberOfMoves;
    public float duration;
    public float slowDuration;

    private Rigidbody2D rb;
    private Vector2 velocity;

    // Use this for initialization
    void Awake () {
        rb = GetComponent<Rigidbody2D>();
        velocity = new Vector2(0, 0);
        StartCoroutine("MoveAround");
    }

    private IEnumerator MoveAround() {
        float timestamp = Time.time;
        while (timestamp + duration > Time.time) {
            if (numberOfMoves > 0.0f)
            {
                velocity.Set(burstSpeed, rb.velocity.y);
                rb.velocity = velocity;
                yield return new WaitForSeconds(moveCooldown);
                numberOfMoves -= 1;
            }
            else
            {
                flip();
                numberOfMoves = numberOfMoves + (float)3.0;
            }
        }
        Destroy(this.gameObject);
    }

    //flip the enemy
    void flip()
    {
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
        burstSpeed *= -1;
    }

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag.Equals("Red")) {
			ScoreTracker.BlueScore++;
			ScoreTracker.RedScore--;
			StartCoroutine("Slow", coll.gameObject);
		}
		if (coll.gameObject.tag.Equals("Blue")) {
			ScoreTracker.BlueScore--;
			ScoreTracker.RedScore++;
			StartCoroutine("Slow", coll.gameObject);
		}
	}

	public IEnumerator Slow(GameObject target) {
		if (target.tag.Equals("Red")) {
			RedPlayerController con = target.GetComponent<RedPlayerController>();
			con.setSlowed(true);
		}
		if (target.tag.Equals("Blue")) {
			BluePlayerController con = target.GetComponent<BluePlayerController>();
			con.setSlowed(true);
		}
		yield return new WaitForSeconds(slowDuration);
		if (target.tag.Equals("Red")) {
			RedPlayerController con = target.GetComponent<RedPlayerController>();
			con.setSlowed(false);
		}
		if (target.tag.Equals("Blue")) {
			BluePlayerController con = target.GetComponent<BluePlayerController>();
			con.setSlowed(false);
		}
    }
}
