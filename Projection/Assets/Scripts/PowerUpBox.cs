﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpBox : MonoBehaviour {

	public PowerUp[] powerUps;

	void OnTriggerEnter2D(Collider2D coll) {
		Debug.Log("PowerUp");
		if (coll.gameObject.tag.Equals("Red") || coll.gameObject.tag.Equals("Blue")) {
			PowerUpHandler pwh = coll.gameObject.GetComponent<PowerUpHandler>();
			int index = Random.Range(0, 3);
			pwh.setPowerUp(powerUps[index]);
			Destroy(this.gameObject);
			
			if (coll.gameObject.tag.Equals("Red")) {
				ScoreTracker.RedScore++;
			}
			if (coll.gameObject.tag.Equals("Blue")) {
				ScoreTracker.BlueScore++;
			}
		}
	}
}
