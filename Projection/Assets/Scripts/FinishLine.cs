﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishLine : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag.Equals("Red")) {
			ScoreTracker.RedScore += 5;
			SceneManager.LoadScene("MainMenu");
		}
		if (coll.gameObject.tag.Equals("Blue")) {
			ScoreTracker.BlueScore += 5;
			SceneManager.LoadScene("MainMenu");
		}
	}
}
