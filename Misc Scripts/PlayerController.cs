﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private Rigidbody2D rb;
    private GroundedChecker gc;

    public float runSpeed;
    public float jumpSpeed = 11.25f; //works well with gravity scale = 5
    public float slideSpeed;
    public float fallMultiplier;
    public float lowJumpMultiplier;
    public float fallSpeed;
    public float airSpeed;
    public float wallJumpMultiplier;

    private bool facingRight = true;
    private bool isDamaged;
    private Vector2 velocity;


	// Use this for initialization
	void Start () {

        rb = GetComponent<Rigidbody2D>();

        gc = GetComponent<GroundedChecker>();

	}
	
	// Update is called once per frame
	void FixedUpdate () {


        moveAndJump();

        //print(gc.isWallStuck());


	}


    void moveAndJump()
    {

        

        //move left and right
        Vector2 newVelocity = rb.velocity;


        //****************move horizontally*****************//


        //assign button for horizontal movement
        float hMove = Input.GetAxis("Horizontal");

        //set x velocity
        newVelocity.x = hMove * runSpeed;



        //******************jump*************//

        //assign the jump input
        //float vMove = Input.GetAxis("Jump");
        bool jumpInput = Input.GetKeyDown(KeyCode.Space);

        //move upwards if pressing the space button and on the ground.
        if (jumpInput && gc.isGrounded())
        {
            newVelocity.y = jumpSpeed;
            //newVelocity += Vector2.up * jumpSpeed;
        }

        //wall jumping
        if (vMove > 0 && gc.isWallStuckRight() && hMove < 0 && !gc.isGrounded())
        {
            newVelocity.y = jumpSpeed * 1.2f;
            newVelocity.x = -1 * runSpeed * wallJumpMultiplier;
                
        }

        if (vMove > 0 && gc.isWallStuckLeft() && hMove > 0 && !gc.isGrounded())
        {
            newVelocity.y = jumpSpeed * 1.2f;
            newVelocity.x = runSpeed * wallJumpMultiplier;
        }

        //if falling, fall faster
        if (newVelocity.y < 0)
        {
            newVelocity += Vector2.up * Physics2D.gravity.y * fallSpeed * Time.fixedDeltaTime;// * (fallMultiplier - 1);
        }

        if (newVelocity.y > 0 && vMove == 0)
        {

            newVelocity += Vector2.up * Physics2D.gravity.y * fallSpeed * Time.fixedDeltaTime * (lowJumpMultiplier - 1);

        }

        //else if (velocity.y > 0 && vMove == 0)
        //{
            //velocity += Vector2.up * Physics2D.gravity.y * Time.fixedDeltaTime;// * (lowJumpMultiplier - 1);
        //}

        /*
        if (vMove > 0)
        {
            newVelocity = vMove * jumpSpeed * Vector2.up;
        }
        else
        {
            newVelocity = Vector2.down * jumpSpeed;
        }
        */

        

        //if (!gc.isGrounded())
        //{
        //    newVelocity.x = hMove * airSpeed;
        //}


        //************************************************//

        //set player velocity
        rb.velocity = newVelocity;
        

    }

}
