﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundedChecker : MonoBehaviour {

    private bool grounded;
    private bool wallStuckRight;
    private bool wallStuckLeft;
    public LayerMask mask; // LayerMask to only check Ground layer.
    private Vector3 correction;
    private Vector3 wallCorrection;

    // Use this for initialization
    void Awake() {
        grounded = false;
        correction = new Vector3(0, -0.4f, 0); // Correction for pivot point. Moves Raycast origin near feet.
        wallCorrection = new Vector3(0, -0.4f, 0);
    }

    // Update is called once per frame
    void FixedUpdate() {
        //check if on ground
        RaycastHit2D hit = Physics2D.Raycast(transform.position - correction, -Vector2.up, 1, mask);
        if (hit.collider != null) {
            grounded = true;
        }
        else {
            grounded = false;
        }

        //check if next to wall
        RaycastHit2D hitWallRight = Physics2D.Raycast(transform.position + wallCorrection, Vector2.right, 0.2f, mask);
        if (hitWallRight.collider != null)
        {
            wallStuckRight = true;
        }
        else
        {
            wallStuckRight = false;
        }

        RaycastHit2D hitWallLeft = Physics2D.Raycast(transform.position + wallCorrection, Vector2.left, 0.2f, mask);
        if (hitWallLeft.collider != null)
        {
            wallStuckLeft = true;
        }
        else
        {
            wallStuckLeft = false;
        }
    }

    public bool isGrounded() {
        return grounded;
    }

    public bool isWallStuckRight()
    {
        return wallStuckRight;
    }

    public bool isWallStuckLeft()
    {
        return wallStuckLeft;
    }
}
